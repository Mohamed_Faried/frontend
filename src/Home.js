import React from 'react';
import { Link } from 'react-router-dom'
class Home extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            user: JSON.parse(localStorage.getItem("UserData")),

        }
    }

    render() {
        return (

            <React.Fragment>
                <div className="container">
                    <div className="row justify-content-md-center">

                        {
                            this.state.user.user_type_id === 1 ? <Link to='/CreateUser'><button>Create User</button></Link> :
                                <React.Fragment>
                                    Welcome  {this.state.user.name}
                                </React.Fragment>
                        }

                    </div>
                </div>


            </React.Fragment>
        );
    }


} export default Home