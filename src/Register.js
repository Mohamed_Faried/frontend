import React from 'react';
import Api from './Api';
class Register extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email : '',
            password : '',
            type : 'doner'
        };
      }

      create(e)
      {
          e.preventDefault();
          Api('/auth/register','post',{},this.state,{}).then(
              response => {
                  
                  if(response.status === 201)
                  {
                      localStorage.setItem("UserData",JSON.stringify(response.data))
                     this.props.history.push('/Home')
                  }
              }
          )
      }
      render() {
          
        return (
            <React.Fragment>
                <div className="container">
                 <div className="row justify-content-md-center">
                <form onSubmit={
                    this.create.bind(this)
                }>
                    <div className="form-group">
                        <label htmlFor="formGroupExampleInput">Name</label>
                        <input type="text" className="form-control" id="formGroupExampleInput"value={this.state.name} onChange={(e)=>{this.setState({name : e.target.value})}} required />
                        </div>
                        <div className="form-group">
                        <label htmlFor="formGroupExampleInput1">Email</label>
                        <input type="text" className="form-control" id="formGroupExampleInput1"value={this.state.email} onChange={(e)=>{this.setState({email : e.target.value})}} required />
                        </div>
                        <div className="form-group">
                            <label htmlFor="formGroupExampleInput2" >Password</label>
                            <input type="password"  className="form-control" id="formGroupExampleInput2" value={this.state.password} onChange={(e)=>{this.setState({password : e.target.value})}} required />
                        </div>
                        <input type="submit" />
             </form>
             </div>
             </div>
            </React.Fragment>
                    );
                }

            


    }export default Register;