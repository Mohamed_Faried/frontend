import React from "react";
import Api from "./Api";
import { Link } from 'react-router-dom';

export class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }

  create(e) {
    e.preventDefault();
    Api('/auth/login', 'post', {}, this.state, {}).then(
      response => {
        if (response.status === 200) {
          localStorage.setItem("UserData", JSON.stringify(response.data));
          this.props.history.push("/Home");
        }
      }
    )
  }
  render() {

    return (
      <form onSubmit={
        this.create.bind(this)
      }>
        <label htmlFor="email">Email</label>
        <input
          name="email"
          type="text"
          placeholder="Enter your email"
          value={this.state.email}
          onChange={(e) => { this.setState({ email: e.target.value }) }} required
        />
        <label htmlFor="email">Password</label>
        <input
          name="password"
          type="password"
          placeholder="Enter your password"
          value={this.state.password}
          onChange={(e) => { this.setState({ password: e.target.value }) }} required
        />
        <button type="submit">Login</button> <Link to='/register'><button type="submit">Signup</button> </Link>
      </form>
    );
  }
} export default Login;
