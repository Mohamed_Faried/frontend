import axios from 'axios';
const Api = (url, method, header, body, params) => {
      return axios({
            baseURL: `http://127.0.0.1:8000/api`,
            method: method,
            url: url,
            data: body,
            headers: header,
            params: params
      });
}
export default Api;