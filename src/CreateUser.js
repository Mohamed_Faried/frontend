import React from 'react';
import Api from './Api';
import 'bootstrap/dist/css/bootstrap.css';
class CreateUser extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: '',
            password: '',
            type: 'admin'
        };
    }

    create(e) {

        e.preventDefault();
        const Headers = {
            Authorization: 'Bearer ' + JSON.parse(localStorage.getItem('UserData')).api_token
        }
        Api('/auth/create-user', 'POST', Headers, this.state, {}).then(
            response => {

                if (response.status === 201) {
                    console.log("User Created Successfully");

                }
            }
        )
    }
    render() {
        return (
            <React.Fragment>
                <div className="container">
                    <div className="row justify-content-md-center">
                        <form onSubmit={
                            this.create.bind(this)
                        }>
                            <div className="form-group">
                                <label htmlFor="formGroupExampleInput">Name</label>
                                <input type="text" className="form-control" id="formGroupExampleInput" value={this.state.name} onChange={(e) => { this.setState({ name: e.target.value }) }} required />
                            </div>
                            <div className="form-group">
                                <label htmlFor="formGroupExampleInput1">Email</label>
                                <input type="text" className="form-control" id="formGroupExampleInput1" value={this.state.email} onChange={(e) => { this.setState({ email: e.target.value }) }} required />
                            </div>
                            <div className="form-group">
                                <label htmlFor="formGroupExampleInput2" >Password</label>
                                <input type="password" className="form-control" id="formGroupExampleInput2" value={this.state.password} onChange={(e) => { this.setState({ password: e.target.value }) }} required />
                            </div>
                            <div className="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadio1" name="customRadio" className="custom-control-input" value='admin' onChange={(e) => { this.setState({ type: e.target.value }) }} checked={this.state.type === 'admin'} />
                                <label className="custom-control-label" htmlFor="customRadio1">Admin</label>
                            </div>
                            <div className="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadio2" name="customRadio" className="custom-control-input" value="doner" onChange={(e) => { this.setState({ type: e.target.value }) }} checked={this.state.type === 'doner'} />
                                <label className="custom-control-label" htmlFor="customRadio2">Doner</label>
                            </div>
                            <br></br>
                            <br></br>
                            <input type="submit" />
                        </form>
                    </div>
                </div>

            </React.Fragment>

        );
    }

} export default CreateUser;